package com.wuwit.convertit.database;

/**
 * Created by QWERTY on 17.05.2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 7;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_ORG = "organization";

    // Login Table Columns names
    private static final String KEY_IDROW = "idrow";
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    //private static final String KEY_REGION = "region_id";
    private static final String KEY_EURO_ASK = "eur_ask";
    private static final String KEY_EURO_BID = "eur_bid";
    private static final String KEY_USD_ASK = "usd_ask";
    private static final String KEY_USD_BID = "usd_bid";
    private static final String KEY_RUB_ASK = "rub_ask";
    private static final String KEY_RUB_BID = "rub_bid";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_ORG + "("
                + KEY_IDROW + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ID + " TEXT," + KEY_TITLE + " TEXT,"
               /* + KEY_REGION + " TEXT,"*/ + KEY_EURO_ASK + " TEXT,"
                + KEY_EURO_BID + " TEXT," + KEY_USD_ASK + " TEXT,"
                + KEY_USD_BID + " TEXT," + KEY_RUB_ASK + " TEXT,"
                + KEY_RUB_BID + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORG);
        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addOrg(String id, String title,/* String region_id,*/ String eur_ask, String eur_bid,
                       String usd_ask, String usd_bid, String rub_ask, String rub_bid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, id); //id
        values.put(KEY_TITLE, title); // Name
       // values.put(KEY_REGION, region_id); //region
        values.put(KEY_EURO_ASK, eur_ask); //euro ask
        values.put(KEY_EURO_BID, eur_bid); //euro bid
        values.put(KEY_USD_ASK,usd_ask);   //dollar ask
        values.put(KEY_USD_BID,usd_bid);   //dollar bid
        values.put(KEY_RUB_ASK,rub_ask);   //rubl ask
        values.put(KEY_RUB_BID, rub_bid);   //rubl bid
        // Inserting Row

        long id_a = db.insert(TABLE_ORG, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New org inserted into sqlite: " +id_a);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails(int pos) {
        HashMap<String, String> org = new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_ORG + " WHERE "+ KEY_IDROW + " = " + pos;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            org.put("idrow", cursor.getString(0));
            org.put("id", cursor.getString(1));
            org.put("title", cursor.getString(2));
            org.put("euro_ask", cursor.getString(3));
            org.put("euro_bid", cursor.getString(4));
            org.put("usd_ask", cursor.getString(5));
            org.put("usd_bid", cursor.getString(6));
            org.put("rub_ask", cursor.getString(7));
            org.put("rub_bid", cursor.getString(8));
        }

        cursor.close();
        db.close();
        // return org
        Log.d(TAG, "Fetching organization from Sqlite: " + org.toString());

        return org;
    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteOgrs() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_ORG, null, null);
        db.close();

        Log.d(TAG, "Deleted all organizations info from sqlite");
    }

}
