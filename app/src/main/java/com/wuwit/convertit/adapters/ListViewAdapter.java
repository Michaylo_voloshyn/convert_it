package com.wuwit.convertit.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wuwit.convertit.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by BOSS on 17.05.2016.
 */
public class ListViewAdapter extends BaseAdapter {
    ArrayList<String> listTitle, listAsk, listBid;
    TextView title, ask, bid;
LayoutInflater inflater;
    private Activity activity;

    public ListViewAdapter(Context context, ArrayList<String> listTitle, ArrayList<String> listAsk, ArrayList<String> listBid) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.listTitle = listTitle;
        this.listAsk = listAsk;
        this.listBid = listBid;
    }

    @Override
    public int getCount() {
        if( listTitle == null)
            return 0;
        else
            return listTitle.size();
    }

    @Override
    public Object getItem(int position) {
        return listTitle.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder
    {

        public TextView listTitle, listAsk, listBid;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view ;

        if(convertView == null) {
            view = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_row,parent,false);


            view.listTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
            view.listAsk = (TextView) convertView.findViewById(R.id.textViewAsk);
            view.listBid = (TextView) convertView.findViewById(R.id.textViewBid);
            convertView.setTag(view);

        } else {
            view = (ViewHolder) convertView.getTag();
        }
        view.listTitle.setText(listTitle.get(position));
        view.listAsk.setText(listAsk.get(position));
        view.listBid.setText(listBid.get(position));


        return convertView;
    }
}
