package com.wuwit.convertit;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.wuwit.convertit.database.SQLiteHandler;
import com.wuwit.convertit.adapters.ListViewAdapter;
import com.wuwit.convertit.adapters.ViewPagerAdapter;
import com.wuwit.convertit.support.AppController;
import com.wuwit.convertit.support.ItemObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {/*
    final static String urlPBexchangejs = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";
    final static String urlNBUexchangejs = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=3";*/
    final static String urlFinanceUajs = "http://resources.finance.ua/ua/public/currency-cash.json";
    private ProgressBar progressBarHome;
    private SQLiteHandler db;

    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    TabLayout tabLayout;


    ArrayList<String> listTitle, listUSDAsk, listUSDBid, listEURask,listEURbid,listRUBask,listRUBbid;
    private ListViewAdapter lvAdapter;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setSupportActionBar(toolbar);
        progressBarHome = (ProgressBar)findViewById(R.id.home_progress);
        listTitle = new ArrayList<>();
        listUSDAsk= new ArrayList<>();
        listUSDBid= new ArrayList<>();
        listEURask= new ArrayList<>();
        listEURbid= new ArrayList<>();
        listRUBask= new ArrayList<>();
        listRUBbid= new ArrayList<>();







        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new listFragment(listTitle,listUSDAsk,listUSDBid), getString(R.string.dolars));
        viewPagerAdapter.addFragment(new listFragment(listTitle,listEURask,listEURbid), getString(R.string.euro));
        viewPagerAdapter.addFragment(new listFragment(listTitle,listRUBask,listRUBbid), getString(R.string.rubl));
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        db = new SQLiteHandler(getApplicationContext());
        getPBexchange();
        Log.d("exc", "--------------------");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getPBexchange(){
        final List<ItemObject> orgList = new ArrayList<>();
        showProgress(true);
        JsonObjectRequest  jsonObjectRequest = new JsonObjectRequest (Request.Method.GET, urlFinanceUajs,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject s) {
                        try {
                            db.deleteOgrs();
                            Log.i("Response", s.toString());
                            JSONArray jsonArray = s.getJSONArray("organizations");
                            Log.i("organizations: ", Integer.toString(jsonArray.length()));
                            String id;
                            String title;
                            String usdAsk;
                            String usdBid;
                            String eurAsk;
                            String eurBid;
                            String rubAsk;
                            String rubBid;
                            JSONObject jsonObject;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Map currencies = new HashMap<>();
                                jsonObject = jsonArray.getJSONObject(i);
                                id = jsonObject.getString("id");
                                title = jsonObject.getString("title");
                                JSONObject curr = jsonObject.getJSONObject("currencies");
                                try {
                                    usdAsk = curr.getJSONObject("USD").getString("ask");
                                    usdBid = curr.getJSONObject("USD").getString("bid");
                                    currencies.put(usdAsk, usdBid);
                                    eurAsk = curr.getJSONObject("EUR").getString("ask");
                                    eurBid = curr.getJSONObject("EUR").getString("bid");
                                    currencies.put(eurAsk,eurBid);
                                    rubAsk = curr.getJSONObject("RUB").getString("ask");
                                    rubBid = curr.getJSONObject("RUB").getString("bid");
                                    currencies.put(rubAsk,rubBid);
                                }catch (JSONException e){
                                    continue;
                                }
                                listTitle.add(title);
                                listUSDBid.add(usdBid);
                                listUSDAsk.add(usdAsk);
                                listEURask.add(eurAsk);
                                listEURbid.add(eurBid);
                                listRUBask.add(rubAsk);
                                listRUBbid.add(rubBid);
                                orgList.add(new ItemObject(title,/* regionId,*/ currencies));
                                db.addOrg(id, title,/* regionId,*/ eurAsk, eurBid, usdAsk, usdBid, rubAsk, rubBid);

                                Log.d("Parse","id: " + id + " title: "+title+ /*" regionId: "+regionId +*/"\n"+ currencies.toString());

                            }
                           /* Log.d("HHH",Integer.toString(orgList.size()));
                            HashMap<String, String> org = db.getUserDetails(100);
                            Log.d("HHH", org.toString());*/
                        }catch (JSONException ex){
                            ex.printStackTrace();
                            Toast.makeText(getApplication(),"Error: connection to server failed! Try again later", Toast.LENGTH_LONG).show();
                        }
                        showProgress(false);
                        viewPagerAdapter.notifyDataSetChanged();
                                            }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showProgress(false);
                if (volleyError.networkResponse != null) {
                    Log.d("Error Response code " , String.valueOf(volleyError.networkResponse.statusCode));
                }

                NetworkResponse response = volleyError.networkResponse;
                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                    Log.i("Error Response code",response.toString());
                    Log.i("Error Response code", Arrays.toString(response.data));
                }

            }
        }){
           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }*/

            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("key","1234");
                return params;
            }*/
        };

        int socketTimeout = 10000; //10 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);

      /*  return modelProductsList;*/

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            progressBarHome.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBarHome.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBarHome.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressBarHome.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}
