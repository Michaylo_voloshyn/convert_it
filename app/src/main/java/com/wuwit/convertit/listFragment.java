package com.wuwit.convertit;

import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.wuwit.convertit.R;
import com.wuwit.convertit.adapters.ListViewAdapter;

import java.util.ArrayList;


/**
 * Created by BOSS on 17.05.2016.
 */
public class listFragment extends Fragment {
    ArrayList<String> listTitle, listAsk, listBid;
    ListView listView;
    View view;

public listFragment(){

}
public listFragment( ArrayList<String> listTitle, ArrayList<String> listAsk, ArrayList<String> listBid){
    this.listTitle = listTitle;
    this.listAsk = listAsk;
    this.listBid = listBid;
}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view ==null)
            view = inflater.inflate(R.layout.list_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        ListViewAdapter adapter = new ListViewAdapter(getActivity().getApplicationContext(),listTitle,listAsk,listBid);
        listView.setAdapter(adapter);
        return view;
    }



}
